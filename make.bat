@echo off

..\..\bin\lcc -Wa-l -c -o jkit.o jkit.c

rem 一度コンパイルしたら、rem 化すると高速に make できる
echo now making font data... please wait few minutes
..\..\bin\lcc -Wa-l -Wf-bo1 -c -o font_data\f01.o font_data\font01.c
..\..\bin\lcc -Wa-l -Wf-bo2 -c -o font_data\f02.o font_data\font02.c
..\..\bin\lcc -Wa-l -Wf-bo3 -c -o font_data\f03.o font_data\font03.c
..\..\bin\lcc -Wa-l -Wf-bo4 -c -o font_data\f04.o font_data\font04.c
rem ここまで rem 化する


..\..\bin\lcc -Wl-m -Wl-yt3 -Wl-yo32 -Wl-yp0x143=0x80 -o jkit.gb jkit.o font_data\f01.o font_data\f02.o font_data\f03.o font_data\f04.o

del *.o
del *.lst
rem del *.map
