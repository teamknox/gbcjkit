#include <stdio.h>
/*
#include <unistd.h>
*/
#include <io.h>
#include <fcntl.h>

/* Constants */
#define CHARNUM 2048               // Number of characters per file.
#define CHARSIZE 8                 // Size of charactor
#define FSIZE (CHARNUM * CHARSIZE) // Data size per file

#define PATH_MAX 128

/* Function Prototype */
FILE *font_fopen(int *fnum,int in_fsize,int counter);
void create_fheader(int fnum,int in_fsize,int counter,FILE *fp);
void font_fclose(FILE *fp,int in_fsize,int counter);

/*******************************************************************************
    main routine
*******************************************************************************/
int main(int argc, char *argv[])
    {
    unsigned char buf[BUFSIZ];
    int in_fd;
    int rlen;
    int i;
    int in_fsize;
    int counter;
    int fnum = 1;
    FILE *fp = NULL;

    if( argc != 2 )
        {
	fprintf(stderr,"usage:%s <FONT_FILE>\n",argv[0]);
	return(1);
	}

    if((in_fd = open(argv[1],O_RDONLY | O_BINARY )) < 0)
        {
	perror("open");
	return(in_fd);
	}

    /****************************
     * Getting input file size  *
     ****************************/
    in_fsize = 0;
    while((rlen = read(in_fd,buf,BUFSIZ)) > 0)
        {
	in_fsize += rlen;
	}

    close(in_fd);

    if((in_fd = open(argv[1],O_RDONLY | O_BINARY )) < 0)
        {
	perror("open");
	return(in_fd);
	}

    if( rlen < 0 )
        {
        perror("Checking file size");
        return(rlen);
        }

    counter = 0;
    while((rlen = read(in_fd,buf,BUFSIZ)) > 0)
        {
	for(i = 0; i < rlen; i++)
	    {

	    /* change file condition   */
	    if(!(counter % (FSIZE)) ) 
	        {
		font_fclose(fp,in_fsize,counter);

                if((fp = font_fopen(&fnum,in_fsize,counter)) == NULL)
                    {
               	    perror("fopen");
               	    return(1);
               	    }
                 }

	    fprintf(fp,"0x%02X,",buf[i]);

            if( !((counter - (CHARSIZE - 1)) % CHARSIZE) ) /*NewLine condition*/
                {
		/* print index condition */
                if( !((counter - (CHARSIZE - 1)) % (0x100 * CHARSIZE)))
                    {
                    fprintf(fp,"  // 0x%04X",(counter - CHARSIZE + 1)/CHARSIZE);
                    }

                fprintf(fp,"\n    ");
	        }

	    counter++;
	    } /* for */

        } /* while( read ) */

    if(rlen < 0)
        {
	perror("read");
	}

    font_fclose(fp,in_fsize,counter); // terminator

    close(in_fd);

    return(rlen);
    }


/*******************************************************************************
*******************************************************************************/
FILE *font_fopen(int *fnum,int in_fsize,int counter)
    {
    char fname[PATH_MAX];
    FILE *fp;

    sprintf(fname,"font%02d.c",*fnum);
    if((fp = fopen(fname,"w")) != NULL)
        {
	create_fheader((*fnum)++,in_fsize,counter,fp);
	}

    return(fp);
    }


/*******************************************************************************
*******************************************************************************/
void create_fheader(int fnum,int in_fsize,int counter,FILE *fp)
    {
    int out_fsize = in_fsize - counter;

    if( out_fsize > FSIZE )
        {
	out_fsize = FSIZE;
	}

    fprintf(fp,"/*************************/\n");
    fprintf(fp,"/* Japanese Font #%02d     */\n",fnum);
    fprintf(fp,"/*                       */\n");
    fprintf(fp,"/* code: 0x%04X - 0x%04X */\n",
        (fnum - 1) * CHARNUM,
	((fnum - 1) * CHARNUM) + (out_fsize / CHARSIZE) - 1);
    fprintf(fp,"/* %4d characters, %2dKB */\n",out_fsize / CHARSIZE,
        (out_fsize) / 0x400);
    fprintf(fp,"/*************************/\n");
    fprintf(fp,"\n");
    fprintf(fp,"unsigned char j_font%02d[] = {\n",fnum);
    fprintf(fp,"    ");
    }


/*******************************************************************************
*******************************************************************************/
void font_fclose(FILE *fp,int in_fsize,int counter)
    {
    int i,j;

    if( fp )
        {
	if( counter == in_fsize ) // all data has been processed.
	    {
	    fseek(fp,-6,SEEK_CUR);
	    fprintf(fp,"  //  0x%04X\n",(counter - 1) / CHARSIZE);
	    fprintf(fp,"    /* END OF DATA */\n");
	    fprintf(fp,"    ");
	    
	    for(i = 0; i < 3; i++)
	        {
		for(j=0; j < CHARSIZE; j++)
		    {
                    fprintf(fp,"0xFF,");
		    }
                fprintf(fp,"\n    ");
                }
	    }

	fseek(fp,-7,SEEK_CUR);
	fprintf(fp,"\n};"); 
	
	fclose(fp);
	}
    }
