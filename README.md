# Kanji driver kit for Gameboy color
Kanji handling in GB/GBC with [ELISA-Font](http://www.vector.co.jp/soft/data/writing/se013782.html). This kit will be integrated to main application such as GB-PDA and so on.

<table>
<tr>
<td><img src="./pics/jkit.jpg"></td>
</tr>
</table>

## ELISA Font
ELISA Font can realize Kanji-characters by 8x8 pixels. Up to JIS-2nd standard kanji inclueded.

## How to use
At 1stly, you have to define C-language array in GB/GBC programming. It means ELISA-Font directly has binary structured.  I contacted to [this site](http://hp.vector.co.jp/authors/VA002310/) However, mail was returned as unknown receiver. Therefore, we decided that font files are prepared by each user with our font converter. The fontconverter is b2cstr which was developed by colleague of team member. At first, the b2cstr was developed on UNIX environment, (after ported to DOS environment.) you can see very [interesting message](./pics/font_tool.jpg) from GBDK. 

### 1. Download GB-JKIT
Here, Download and unzip GB-JKIT to your target folder. 

### 2. Font file conversion
Download ELISA FONT. Extract ELISA100.FNT . You can find out b2cstr.exe in font_tool folder. From commandline
```
b2cstr ELISA100.FNT[Enter]
```
Success appeared [font01.c - font04.c](./pics/font_data.jpg). 

### 3. Make folder
Make [font_data Folder](./pics/font_data.jpg) which is in GB-JKIT, then move converted files to here.

### 4. Build
OK, excute "make.bat". We need few minute for make up. 4 banks will be used for strings.

### 5. After 2nd Build
You should link [object of font files](./pics/font_data_o.jpg) to your home-brew software.

## Caution
This software will be used in home-brew software. Therefore, use only either Programmer or expert. We can not accept any question for this issue. 

## Thanks to
Mr.K.K is ex-colleague of Osamu OHASHI.

# License
Copyright (c) Osamu OHASHI  
Distributed under the MIT License either version 1.0 or any later version. 
 
