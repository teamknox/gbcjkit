/****************************************************************************
 *  jkit.c                                                                  *
 *                                                                          *
 *    Japanese font extension kit for GB.     V1.00    2001/03/20           *
 *                                                                          *
 *                                                                          *
 *    Copyright(C)  2001   TeamKNOx                                         *
 ****************************************************************************/

#include <gb.h>
#include <stdlib.h>

#include "bkg.h"
#include "bkg.c"

#define POS_X     1
#define POS_Y     3
#define SIZE_X    18
#define SIZE_Y    10
#define SEL_X     9
#define SEL_Y     10
#define SIZE_COL  (SIZE_X*8/SEL_X)
#define SIZE_ROW	(SIZE_Y*8/SEL_Y)


/***************************************************************************
 *  display screen                                                         *
 ***************************************************************************/
unsigned char disp_map[18][20] = {
        "Japanese Font Viewer", // 0
        "   (C) 2001 TeamKNOx", // 1
        "--------------------", // 2
        "                    ", // 3  window size  18 x 10
        "                    ", // 4
        "                    ", // 5
        "                    ", // 6
        "                    ", // 7
        "                    ", // 8
        "                    ", // 9
        "                    ", // 10
        "                    ", // 11
        "                    ", // 12
        "--------------------", // 13
        "SJIS:               ", // 14
        "JIS:                ", // 15
        "INDX:               ", // 16
        "BNK:      ADR:      "  // 17
};
UBYTE mask[8] = { 0x00, 0x80, 0xC0, 0xE0, 0xF0, 0xF8, 0xFC, 0xFE };
unsigned char mix_a[]  = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };
unsigned char fore_a[] = { 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1 };
unsigned char back_a[] = { 2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2 };
UWORD palette_0[] = { 0xFFFF, 0xFFFF, 0xFFFF, 0x0000 };
UWORD palette_1[] = { 0xFFFF, 0x0000, 0xFFFF, 0x0000 };
UWORD palette_2[] = { 0xFFFF, 0xFFFF, 0x0000, 0x0000 };

UBYTE  pos_x, pos_y;
UBYTE  v_bk[10][18];
unsigned char *v_ad[10][18];

void set_bkg_attribute( UBYTE x, UBYTE y, UBYTE sx, UBYTE sy, unsigned char *p )
{
    if( _cpu == CGB_TYPE ) {
        VBK_REG = 1;
        set_bkg_tiles( x, y, sx, sy, p );
        VBK_REG = 0;
    }
}

void print_hex( UBYTE x, UBYTE y, UWORD v )
{
    UBYTE  i;
    unsigned char tmp[4];

    tmp[3] = v & 0x0F;  v = v / 16;
    tmp[2] = v & 0x0F;  v = v / 16;
    tmp[1] = v & 0x0F;  v = v / 16;
    tmp[0] = v & 0x0F;
    for( i=0; i<4; i++ ) {
        if( tmp[i] > 9 )  tmp[i] += '7';
        else              tmp[i] += '0';
    }
    set_bkg_tiles( x, y, 4, 1, tmp );
}

void inc_pos()
{
    if( pos_x < 17 )  pos_x++;
    else {
        pos_x = 0;
        if( pos_y < 9 ) pos_y++;
        else            pos_y = 0;
    }
}
void set_pos( UBYTE x, UBYTE y )
{
    pos_x = x;
    pos_y = y;
}
UBYTE get_tile_id( UBYTE x, UBYTE y )
{
    return( x+y*18+0x80 );
}

void make_bkg_data( UBYTE x, UBYTE y )
{
    unsigned char img[16];
    UBYTE    i, bk, id;
    unsigned char *p;

    /* copy from rom data to work buff */
    y = y % 5;
    id = get_tile_id( x, y );
    p  = v_ad[y][x];
    SWITCH_ROM_MBC1( v_bk[y][x] );
    for( i=0; i<8; i++ )   img[i*2] = *p++;

    p  = v_ad[5+y][x];
    SWITCH_ROM_MBC1( v_bk[5+y][x] );
    for( i=0; i<8; i++ )   img[i*2+1] = *p++;

    SWITCH_ROM_MBC1( 0 );
    set_bkg_data( id, 1, img );
}

typedef struct conv_tbl {
    UWORD  index;
    UWORD  begin;
    UWORD  end;
} _conv_tbl;

struct conv_tbl jis2gb[] = {
    { 0x0000, 0x2121, 0x218C },  //  1 �L��
    { 0x006C, 0x223A, 0x2241 },  //  2 ���w�L��
    { 0x0074, 0x224A, 0x2251 },  //  3 �㐔�L��
    { 0x007B, 0x225C, 0x226A },  //  4 �����L��
    { 0x008A, 0x2272, 0x2279 },  //  5 ���y�L��
    { 0x0092, 0x227E, 0x227E },  //  6 ����
    { 0x0093, 0x2330, 0x2339 },  //  7 ����
    { 0x009D, 0x2341, 0x235A },  //  8 �p�啶��
    { 0x00B7, 0x2361, 0x237A },  //  9 �p������
    { 0x00D1, 0x2421, 0x2473 },  // 10 �Ђ炪��
    { 0x0124, 0x2521, 0x2576 },  // 11 �J�^�J�i
    { 0x017A, 0x2621, 0x2638 },  // 12 �M���V���啶��
    { 0x0192, 0x2641, 0x2658 },  // 13 �M���V��������
    { 0x01AA, 0x2721, 0x2741 },  // 14 ���V�A�啶��
    { 0x01CB, 0x2751, 0x2771 },  // 15 ���V�A������
    { 0x01EC, 0x2821, 0x2840 },  // 16 �r������
    { 0x020C, 0x3021, 0x4F54 },  // 17 ��ꐅ������
    { 0x0DA1, 0x5021, 0x5D5C }   // 18 ��񐅏�����
};

void conv_jis2gb( UBYTE *bk, unsigned char **p, UWORD jis )
{
    UBYTE  i;
    UWORD  index;

    for( i=0; i<18; i++ ) {
        if( (jis2gb[i].begin <= jis) && (jis <= jis2gb[i].end) ) {
            if( i<16 ) {
                index = (UWORD)jis - (UWORD)jis2gb[i].begin;
            } else {
                index = ((jis/0x0100)-(jis2gb[i].begin/0x0100))*94
                      + ((jis&0x00FF)-(jis2gb[i].begin&0x00FF));
            }
            index = index + jis2gb[i].index;
            print_hex( 6, 16, index );
            *bk = (UBYTE)(index / 0x0800 + 1);
            *p  = (unsigned char *)((index % 0x0800)*8+0x4000);
            return;
        }
    }
    print_hex( 6, 16, 0 );
    *bk = 1;
    *p  = (unsigned char *)0x4000;
}

UWORD conv_sjis2jis( UWORD sjis )
{
    UBYTE  hi, lo;

    hi = (UBYTE)(sjis / 0x0100);
    lo = (UBYTE)(sjis & 0x00FF);

    if( 0x80<=hi && hi<=0x9F ) {
        hi = (hi-0x71)*2+1;
    } else if( 0xE0<=hi && hi<=0xFF ) {
        hi = (hi-0xB1)*2+1;
    } else {
        // ANK ?
    }
    if( lo > 0x7F )  lo--;
#if 0 // debug
    if( lo < 0x9E )  lo -= 0x1F;
#else
    if( lo < 0xAF )  lo -= 0x1F;
#endif
    else {
        hi++;
        lo -= 0x7D;
    }
    sjis = (UWORD)hi;
    sjis = sjis * 0x0100 + lo;
    return( sjis );
}

void print_jis( UWORD code )
{
    UBYTE bk;
    unsigned char *p;

    print_hex( 6, 15, code );

    /* convert jis to gb banked address */
    conv_jis2gb( &bk, &p, code );

    print_hex( 5,  17, (UWORD)bk );
    print_hex( 15, 17, (UWORD)p );

    /* store code */
    v_bk[pos_y][pos_x] = bk;
    v_ad[pos_y][pos_x] = p;

    /* make tile data */
    make_bkg_data( pos_x, pos_y );

    /* inc cursor to next point */
    inc_pos();
}

void printf_jis( UBYTE *p )
{
    UBYTE hi, lo;

    hi = 0;
    while( lo=(UBYTE)*p ) {
        if( hi ) {
            print_jis( (UWORD)hi*0x0100+lo );
            hi = 0;
        } else {
            hi = lo;
        }
        p++;
    }
}

void print_sjis( UWORD code )
{
    UBYTE bk;
    unsigned char *p;

    print_hex( 6, 14, code );

    /* convert sjis to gb banked address */
    code = conv_sjis2jis( code );
    print_jis( code );
}

void printf_sjis( UBYTE *p )
{
    UBYTE hi, lo;

    hi = 0;
    while( lo=(UBYTE)*p ) {
        if( hi ) {
            if( lo>0x7F )  lo--;
#if 0 // debug
            if( lo<0x9E )  lo -= 0x1F;
#else
            if( lo<0xAF )  lo -= 0x1F;
#endif
            else {
                hi++;
                lo -= 0x7D;
            }
            print_jis( (UWORD)hi*0x0100+lo );
            hi = 0;
        } else if( 0x80<=lo && lo<=0x9F ) {
            hi = (lo-0x71)*2+1;
        } else if( 0xE0<=lo && lo<=0xFF ) {
            hi = (lo-0xB1)*2+1;
        } else {
            /* ANK */
//          Lib_draw_ank( drawpos_x, drawpos_y, lo );
        }
        p++;
    }
}

void display()
{
    unsigned char tmp[1];
    UBYTE  x, y;

    set_bkg_data( 0, 128, bkg );
    set_bkg_tiles( 0, 0, 20, 18, &disp_map[0][0] );
    set_bkg_palette( 0, 1, palette_0 );
    set_bkg_palette( 1, 1, palette_1 );
    set_bkg_palette( 2, 1, palette_2 );

    for( y=0; y<10; y++ ) {
        for( x=0; x<18; x++ ) {
            tmp[0] = 0x80 + x + (y%5)*18;
            set_bkg_tiles( 1+x, 3+y, 1, 1, tmp );
            v_bk[y][x] = 1;
            v_ad[y][x] = (unsigned char *)0x4000;
        }
    }
    for( y=0; y<5; y++ ) {
        set_bkg_attribute( 1, 3+y, 18, 1, fore_a );
        set_bkg_attribute( 1, 8+y, 18, 1, back_a );
    }
}

void test_jis_tbl()
{
    UBYTE  x, y;
    UWORD  code;

    while( 1 ) {
        for( y=0x21; y<0x5E; y++ ) {
            set_pos( 0, 0 );
            for( x=0x20; x<0xC0; x++ ) { // 0x7F
                code = (UWORD)y;
                code = code * 0x100 + x;
                print_jis( code );
                while( !(joypad() & J_A) );
                waitpadup();
            }
        }
    }
}

void test_sjis_tbl()
{
    UBYTE  x, y;
    UWORD  code;

    while( 1 ) {
        for( y=0x81; y<0xFD; y++ ) { // 0x81 to 0xFC
            set_pos( 0, 0 );
            for( x=0x60; x<0xFD; x++ ) { // 0x40 to 0xFC
                code = (UWORD)y;
                code = code * 0x100 + x;
                print_sjis( code );
                while( !(joypad() & J_A) );
                waitpadup();
            }
        }
    }
}


/***************************************************************************
 *  program entry point                                                    *
 ***************************************************************************/
void main()
{
    unsigned char *p, tmp[50];
    UBYTE  i, x, y, z, bank;
    UWORD  code;

    /* initialize GB */
    SWITCH_ROM_MBC1( 0 );
    display();
    SHOW_BKG;
    enable_interrupts();
    DISPLAY_ON;

    // test_jis_tbl();
    // test_sjis_tbl();

    /* endless loop */
    while( 1 ) {
        set_pos( 0, 0 );
        for( i=0; i<50; i++ )  tmp[i]=0;
        memcpy( tmp, "���P�O�S�|�O�O�R�P�@�@�@�@�@�@�@�@�@", 40 );
        printf_sjis( (UBYTE *)tmp );
        for( i=0; i<50; i++ )  tmp[i]=0;
        memcpy( tmp, "�@�@�����s�����拞���R�|�P�S�|�U�@�@", 40 );
        printf_sjis( (UBYTE *)tmp );
        for( i=0; i<50; i++ )  tmp[i]=0;
        memcpy( tmp, "�@�@�@�@�@�@�@�@�@�@�֓��r���Q�e�@�@", 40 );
        printf_sjis( (UBYTE *)tmp );
        for( i=0; i<50; i++ )  tmp[i]=0;
        memcpy( tmp, "�@�@������ЎO�˃u�b�N�X�@�@�@�@�@�@", 40 );
        printf_sjis( (UBYTE *)tmp );
        for( i=0; i<50; i++ )  tmp[i]=0;
        memcpy( tmp, "�@�@�@�@�@�@�@�@�@�@�@�@�@�@�@�@�@�@", 40 );
        printf_sjis( (UBYTE *)tmp );
        for( i=0; i<50; i++ )  tmp[i]=0;
        memcpy( tmp, "�@�s�d�k�@�O�R�|�R�T�R�T�|�V�R�R�P�@", 40 );
        printf_sjis( (UBYTE *)tmp );
        for( i=0; i<50; i++ )  tmp[i]=0;
        memcpy( tmp, "�@�e�`�w�@�O�R�|�R�T�U�V�|�U�S�R�T�@", 40 );
        printf_sjis( (UBYTE *)tmp );

        while( !(joypad() & J_A) );
        waitpadup();
    }
}

/* EOF */